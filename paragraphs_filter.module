<?php

/**
 * @file
 * Primary module hooks for Paragraphs Filter module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Alters paragraph add form.
 *
 * Implements hook_form_FORM_ID_alter().
 */
function paragraphs_filter_form_paragraphs_type_add_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  paragraphs_filter_form_paragraphs_type_add_edit_form_alter($form, $form_state, $form_id);
}

/**
 * Alters paragraph edit form.
 *
 * Implements hook_form_FORM_ID_alter().
 */
function paragraphs_filter_form_paragraphs_type_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  paragraphs_filter_form_paragraphs_type_add_edit_form_alter($form, $form_state, $form_id);
}

/**
 * Add content types to the paragraph form.
 */
function paragraphs_filter_form_paragraphs_type_add_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $entityTypeManager = \Drupal::service('entity_type.manager');
  $options = [];
  $contentTypes = $entityTypeManager->getStorage('node_type')->loadMultiple();
  foreach ($contentTypes as $contentType) {
    $options[$contentType->id()] = $contentType->label();
  }

  $entity = $form_state->getFormObject()->getEntity();
  $filled = $entity->isNew() ? [] : \Drupal::config('paragraphs_filter.settings')
    ->get($entity->id());
  $form['content_types'] = [
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#default_value' => $filled ? $filled : [],
    '#options' => $options,
    '#description' => t('Select one or more content types to filter, select nothing to not filter.'),
  ];

  $form['actions']['submit']['#submit'][] = 'paragraphs_filter_form_paragraphs_type_add_form_submit';
}

/**
 * Stores content types for paragraph type to the database.
 */
function paragraphs_filter_form_paragraphs_type_add_form_submit($form, &$form_state) {
  $values = array_filter($form_state->getValue('content_types'));
  if (empty($values)) {
    \Drupal::configFactory()->getEditable('paragraphs_filter.settings')
      ->clear($form_state->getValue('id'))
      ->save();
  }
  else {
    \Drupal::configFactory()->getEditable('paragraphs_filter.settings')
      ->set($form_state->getValue('id'), $values)
      ->save();
  }
}

/**
 * Clear unneeded settings.
 *
 * Implements hook_ENTITY_TYPE_delete().
 */
function paragraphs_filter_paragraphs_type_delete(EntityInterface $entity) {
  \Drupal::configFactory()->getEditable('paragraphs_filter.settings')
    ->clear($entity->id())
    ->save();

  if (empty(\Drupal::config('paragraphs_filter.settings')->get())) {
    \Drupal::configFactory()
      ->getEditable('paragraphs_filter.settings')
      ->delete();
  }
}

/**
 * Filters paragraph list for the content type field.
 *
 * Implements hook_form_FORM_ID_alter().
 */
function paragraphs_filter_form_field_config_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form['#entity']->getEntityTypeId() == 'node' && isset($form['settings']['handler'])) {
    $types = $form['settings']['handler']['handler_settings']['target_bundles']['#options'];
    foreach ($types as $key => $type) {
      $config = \Drupal::config('paragraphs_filter.settings')->get($key);
      if (!empty($config) && !in_array($form['#entity']->bundle(), $config)) {
        unset($form['settings']['handler']['handler_settings']['target_bundles_drag_drop'][$key]);
      }
    }
  }
}

/**
 * Alters paragraphs list page.
 *
 * Implements hook_entity_type_alter().
 */
function paragraphs_filter_entity_type_alter(array &$entity_types) {
  /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */
  $entity_types['paragraphs_type']->setListBuilderClass('Drupal\paragraphs_filter\Controller\ParagraphsFilterListBuilder');
}
