<?php

namespace Drupal\paragraphs_filter\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\paragraphs\Controller\ParagraphsTypeListBuilder;

/**
 * Alters a listing of ParagraphsType.
 */
class ParagraphsFilterListBuilder extends ParagraphsTypeListBuilder {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['form'] = \Drupal::formBuilder()
      ->getForm('Drupal\paragraphs_filter\Form\ParagraphsFilterForm');
    $build += parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = parent::buildHeader();
    $op = array_pop($header);
    $header['content_type'] = $this->t('Content types');
    array_push($header, $op);
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $filled = $entity->isNew() ? [] : \Drupal::config('paragraphs_filter.settings')
      ->get($entity->id());

    $content_type = \Drupal::request()->get('content_type');
    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple($filled);
    $labels = [];
    foreach ($types as $type) {
      $labels[] = $type->label();
    }

    if (empty($content_type) || empty($filled) || in_array($content_type, $filled)) {
      $row = parent::buildRow($entity);
      $op = array_pop($row);
      $row['content_type']['data'] = ['#markup' => implode('<br>', $labels)];
      array_push($row, $op);
      return $row;
    }

  }

}
