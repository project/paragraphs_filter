<?php

namespace Drupal\paragraphs_filter\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter for paragraph types list page.
 */
class ParagraphsFilterForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ParagraphsFilterForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paragraphs_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = ['All' => $this->t('- Any -')];
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($contentTypes as $contentType) {
      $options[$contentType->id()] = $contentType->label();
    }

    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#options' => $options,
      '#default_value' => $this->getRequest()->get('content_type'),
      '#wrapper_attributes' => [
        'class' => [
          'views-exposed-form__item',
          'views-exposed-form__item--preceding-actions',
        ],
      ],
    ];
    $form['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'class' => [
          'views-exposed-form__item',
          'views-exposed-form__item--actions',
        ],
      ],
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];
    $content_type = $this->getRequest()->get('content_type');
    if (!empty($content_type)) {
      $form['actions']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
      ];
    }
    $form['#attributes']['class'][] = 'views-exposed-form';
    $form['#attached']['library'][] = 'claro/views';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->isValueEmpty('op') &&
      $form_state->getValue('op')->getUntranslatedString() == $this->t('Reset')
        ->getUntranslatedString()) {
      $form_state->setRedirect('entity.paragraphs_type.collection');
      return;
    }
    $options = [];
    $content_type = $form_state->getValue('content_type');
    if ($content_type != 'All') {
      $options = [
        'query' => ['content_type' => $content_type],
      ];
    }
    $form_state->setRedirect('entity.paragraphs_type.collection', [], $options);
  }

}
