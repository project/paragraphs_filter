Paragraphs Filter

This awesome module allows to filter paragraphs by content types. It needs when
you create the site with many paragraph types, and wants to filter
these types for the site administrator. A Paragraphs module is required.

You can assign several content types to single paragraph type. Any content type
which has ERR (Entity Reference Revisions) Paragraphs field will be restricted
by filter. Chained paragraphs (paragraphs with paragraphs inside) don't
restricted by this filter.

(C) 2022 Andrew Answer
https://it.answe.ru | mail@answe.ru | https://t.me/andrew_answer
